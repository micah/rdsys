// Copyright (c) 2021-2022, The Tor Project, Inc.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package whatsapp

import (
	"context"
	"fmt"
	"github.com/mdp/qrterminal"
	"gitlab.torproject.org/tpo/anti-censorship/rdsys/internal"
	"go.mau.fi/whatsmeow"
	waProto "go.mau.fi/whatsmeow/binary/proto"
	"go.mau.fi/whatsmeow/store/sqlstore"
	"go.mau.fi/whatsmeow/types"
	"go.mau.fi/whatsmeow/types/events"
	waLog "go.mau.fi/whatsmeow/util/log"
	"google.golang.org/protobuf/proto"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
)

func InitFrontend(cfg *internal.Config) {
	// Connect to the WhatsApp account and set up event handlers
	err := Connect(cfg)
	if err != nil {
		log.Fatal(err)
	}

	// Wait for a signal to gracefully disconnect
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, os.Interrupt, syscall.SIGTERM)
	<-signalChan

	// Disconnect from the WhatsApp account
	Disconnect()
}

var client *whatsmeow.Client

func Connect(cfg *internal.Config) error {
	// Initialize the database logger
	dbLog := waLog.Stdout("Database", "DEBUG", true)

	//Initialize the database container
	container, err := sqlstore.New("sqlite3", "file:"+cfg.Distributors.Whatsapp.SessionFile+"?_foreign_keys=on", dbLog)
	if err != nil {
		return err
	}
	// Get the first device from the container
	device, err := container.GetFirstDevice()
	if err != nil {
		return err
	}
	// Initialize the client
	clientLog := waLog.Stdout("Client", "INFO", true)
	c := whatsmeow.NewClient(device, clientLog)
	//Set the event handlers
	SetHandlers(c)
	// Connect to WhatsApp
	if c.Store.ID == nil {
		//If the device is not logged in, get the QR code and wait for the user to scan it
		qrChan, _ := c.GetQRChannel(context.Background())
		err = c.Connect()
		if err != nil {
			return err
		}

		for channelItem := range qrChan {
			if channelItem.Event == "code" {
				// Render the QR code here
				qrterminal.GenerateHalfBlock(channelItem.Code, qrterminal.L, os.Stdout)
				// or just manually `echo 2@... | qrencode -t ansiutf8` in a terminal
				fmt.Println("Scan the QR code above to log in", channelItem.Code)
				// you can use the channelItem to render QR code or send it via HTTP, etc
			} else {
				fmt.Println("Login Success", channelItem.Event)
				break
			}
		}
	} else {
		// Already logged in, just connect
		err = c.Connect()
		fmt.Println("Login Success")
		if err != nil {
			return err
		}
	}
	// Store the client for later use
	client = c
	return nil
}

func Disconnect() {
	client.Disconnect()
}

func SetHandlers(c *whatsmeow.Client) {
	c.AddEventHandler(func(evt interface{}) {
		HiHandler(evt, c)
	})
}

func HiHandler(evt interface{}, c *whatsmeow.Client) {
	switch v := evt.(type) {
	case *events.Message:
		msg := strings.ToLower(v.Message.GetConversation())
		switch msg {
		case "hi bot", "hello bot", "hey bot", "oi bot", "olá bot", "hi", "hello":
			err := SendMessage("Hello 👋!", c, v.Info.Chat)
			if err != nil {
				fmt.Println(err.Error())
			}
		}
	}
}

func SendMessage(message string, client *whatsmeow.Client, receiver types.JID) error {
	_, err := client.SendMessage(receiver, "", &waProto.Message{
		Conversation: proto.String(message),
	})
	return err
}
