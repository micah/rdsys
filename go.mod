module gitlab.torproject.org/tpo/anti-censorship/rdsys

go 1.14

require (
	github.com/NullHypothesis/zoossh v0.0.0-20211012143359-017a7be2e713
	github.com/aws/aws-sdk-go-v2 v1.17.7
	github.com/aws/aws-sdk-go-v2/service/s3 v1.17.0
	github.com/emersion/go-imap v1.2.1
	github.com/emersion/go-sasl v0.0.0-20211008083017-0b9dcfb154ac // indirect
	github.com/google/go-github/v50 v50.2.0
	github.com/mdp/qrterminal v1.0.1
	github.com/prometheus/client_golang v1.14.0
	github.com/stretchr/testify v1.8.2
	github.com/xanzy/go-gitlab v0.81.0
	gitlab.torproject.org/tpo/anti-censorship/geoip v0.0.0-20210928150955-7ce4b3d98d01
	go.mau.fi/whatsmeow v0.0.0-20220313125942-12fa61f6a584
	golang.org/x/oauth2 v0.6.0
	google.golang.org/api v0.114.0
	google.golang.org/protobuf v1.30.0
	gopkg.in/telebot.v3 v3.1.3
)
